import {Posts} from "./collections";

Meteor.methods({
    addPosts(post){
        return Posts.insert(post)
    },
    removePost(_id){
        return Posts.remove({_id:_id})
    },
    updatePost(id,isPrivate){
        Posts.update(id,{$set:{'isPrivate': !isPrivate}})
    }
    
})
