import { FlowRouter } from 'meteor/ostrio:flow-router-extra';



FlowRouter.route('/', {
  name: 'login',
  action() {
      
    BlazeLayout.render('MainLayout', {main:'LoginLayout'});

  }
});

// FlowRouter.route('/test', {
//     name:'test',
//   action() {
//     // Show 404 error page using Blaze
//     this.render('MainLayout', {main: 'Test'});

//     // Can be used with BlazeLayout,
//     // and ReactLayout for React-based apps
//   }
// });
