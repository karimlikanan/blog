import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import {Posts} from '../api/login/collections'




Template.LoginLayout.onCreated(function () {
    this.autorun(() => {
        this.subscribe('getPosts')
    })

    this.isPrivate = new ReactiveVar(false);
})

Template.LoginLayout.helpers({
    loggedIn(){
        return Meteor.userId();
    },
    publicPosts(){
        return Posts.find({isPrivate:false});
    },
    privatePosts(){
        return Posts.find({
        userId: Meteor.userId()});
    }
})


Template.LoginLayout.events({
    'submit #addPost'(e, temp) {
        e.preventDefault()

        let text = e.target.postText.value;
        let isPrivate = e.target.check.checked;

        let userId = Meteor.userId();
        let postData = {
            post:text,
            isPrivate: isPrivate,
            userId: userId

        }
        Meteor.call('addPosts', postData, (err, res) =>{
            if(err) console.log(err);
            if(res) console.log(res);
        })
    },
    'click #delete'(e,temp){
        let id = this._id;
        Meteor.call('removePost',id, (err,res) =>{
            if(err) console.log(err);
            if(res) console.log(res);
        })
    },
    'click #pubPriv'(e,temp){
        let id = this._id;
        let isPrivate = this.isPrivate;

        Meteor.call('updatePost',id, isPrivate,  (err,res) =>{
            if(err) console.log(err);
            if(res) console.log(res);
        })
    }

    })